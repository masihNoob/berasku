# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
 export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/rheyfrans/.oh-my-zsh"
export xzsh="/usr/share/zsh/plugins"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment one of the following lines to change the auto-update behavior
# zstyle ':omz:update' mode disabled  # disable automatic updates
zstyle ':omz:update' mode auto      # update automatically without asking
# zstyle ':omz:update' mode reminder  # just remind me to update when it's time

# Uncomment the following line to change how often to auto-update (in days).
# zstyle ':omz:update' frequency 13

# Uncomment the following line if pasting URLs and other text is messed up.
DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# You can also set it to another string to have that shown instead of the default red dots.
# e.g. COMPLETION_WAITING_DOTS="%F{yellow}waiting...%f"
# Caution: this setting can cause issues with multiline prompts in zsh < 5.7.1 (see #5765)
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.


plugins=(
	git
	sudo
	scd
	colorize
	history
	systemd
	archlinux
	rsync
	copybuffer
	dirhistory
	# zsh-completions	
	# zsh-autosuggestions
	# zsh-syntax-highlighting
)

fpath=(/usr/share/zsh/site-functions $fpath)

source $ZSH/oh-my-zsh.sh
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# autoload -Uz compinit promptinit
# compinit
# promptinit

# zstyle ':completion:*' menu select
# zstyle ':completion::complete:*' gain-privileges 1

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh



alias reload='source ~/.zshrc'
alias zshconfig="mate ~/.zshrc"
alias ohmyzsh="mate ~/.oh-my-zsh"

alias cari="pacman -Ss"
alias yari="yay -Ss"
alias udme="sudo pacman -Syyu --overwrite"
alias yudme="yay -Su"
alias cins="sudo pacman -S --needed"
alias yins="yay -S"
alias cuns="sudo pacman -Rsn"
alias yuns="sudo pacman -Scc && sudo rm -r ~/.cache/makepkg/* && sudo rm -r ~/.cache/yay/*"

alias syr="sudo systemctl restart"
alias syt="systemctl status"
alias syx="sudo systemctl stop"

alias sur="systemctl --user restart"
alias sut="systemctl --user status"
alias sux="systemctl --user stop"

alias vsync="xrandr --output eDP-1-1 --set 'PRIME Synchronization'"

alias ytdl="youtube-dl"
alias audl="youtube-dl -x --audio-quality 0 --audio-format mp3"

alias fram='sudo sh -c "/usr/bin/echo 3 > /proc/sys/vm/drop_caches"'

alias setcam='sudo modprobe -r v4l2loopback && sudo modprobe v4l2loopback devices=2 video_nr=0,1 card_label="FHD camera","usb camera"  exclusive_caps=1,1 max_buffers=2'
alias setaud='pactl load-module module-pipe-source source_name=virtmic file=/tmp/virtmic format=s16le rate=16000 channels=1'

alias -g mau='mpv --loop --no-video'

fakecam(){
	ffmpeg -stream_loop -1 -re -i $2 -vcodec rawvideo -pix_fmt yuv420p -framerate 60 -threads 0 -vf hflip -f v4l2 /dev/video0 -f s16le -ar 16000 -ac 1 - > /tmp/virtmic
 }

refresh(){
	 sudo reflector --verbose --country $1 --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
 }

 xuns(){
   local a=$(pacman -Qdtq)
   sudo pacman -Rsn $a
 }
